﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.ComponentModel;

namespace Graphic_creator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();  
        }

        Graphic.GraphType type;
        Graphic graphic;

        private Point GetStart()
        {
            int X;
            int Y;

            int.TryParse(StartX.Text, out X);
            int.TryParse(EndX.Text, out Y);

            Point start = new Point(X, Y);
            return start;

        }
        
        private void DrowGraphButton_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            double c;

            int startX;
            int endX;

            int startY;
            int endY;
            
            double.TryParse(value_A.Text, out a);
            double.TryParse(value_B.Text, out b);
            double.TryParse(value_C.Text, out c);

            int.TryParse(StartX.Text, out startX);
            int.TryParse(EndX.Text, out endX);

            int.TryParse(StartY.Text, out startY);
            int.TryParse(EndY.Text, out endY);

            if ((startX >= endX)||(startY >= endY))
            {
                MessageBox.Show("Начало отрезка должно быть меньше конца");
                return;
            }

            float segmentX = endX - startX;
            float segmentY = endY - startY;

            float zoomX = GraphicField.Width / Math.Abs(segmentX);
            float zoomY = GraphicField.Height / Math.Abs(segmentY);

            Point startpoint = new Point((int)(-1 * segmentX / 2), (int)(-1 * segmentY / 2));

            graphic = new Graphic(zoomX, zoomY, GraphicField.Width, GraphicField.Height, startpoint);

            graphic.GetSegmentX(startX, endX);
            graphic.GetSegmentY(startY, endY);

            GraphicField.Image = new Bitmap(graphic.GetGraphic(type, a, b, c));

            GraphicField.Invalidate();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            GraphicField.Image = null;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveGraphicDialog.ShowDialog();
        }

        private void SaveGraphicDialog_FileOk(object sender, CancelEventArgs e)
        {
            string filename = SaveGraphicDialog.FileName;
            GraphicField.Image.Save(filename);
        }

        private void GraphSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (GraphSelector.SelectedIndex)
            {
                case 0:
                    type = Graphic.GraphType.Sin;
                    break;
                case 1:
                    type = Graphic.GraphType.Cos;
                    break;
                case 2:
                    type = Graphic.GraphType.Tan;
                    break;
                case 3:
                    type = Graphic.GraphType.Ctan;
                    break;
                case 4:
                    type = Graphic.GraphType.Quad;
                    break;
                case 5:
                    type = Graphic.GraphType.Mod;
                    break;
                default:
                    type = Graphic.GraphType.Sin;
                    break;
            }
        }
    }

}
