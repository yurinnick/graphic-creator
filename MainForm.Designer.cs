﻿namespace Graphic_creator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
           } 
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GraphicBox = new System.Windows.Forms.GroupBox();
            this.GraphicField = new System.Windows.Forms.PictureBox();
            this.EraseButton = new System.Windows.Forms.Button();
            this.DrowGraphButton = new System.Windows.Forms.Button();
            this.GraphSelector = new System.Windows.Forms.ComboBox();
            this.Label_A = new System.Windows.Forms.Label();
            this.label_C = new System.Windows.Forms.Label();
            this.label_B = new System.Windows.Forms.Label();
            this.value_A = new System.Windows.Forms.TextBox();
            this.value_B = new System.Windows.Forms.TextBox();
            this.value_C = new System.Windows.Forms.TextBox();
            this.value_Y = new System.Windows.Forms.Label();
            this.SaveGraphicButton = new System.Windows.Forms.Button();
            this.ParametersGroup = new System.Windows.Forms.GroupBox();
            this.GraphicTypeBox = new System.Windows.Forms.GroupBox();
            this.ActionGroup = new System.Windows.Forms.GroupBox();
            this.LabelNewY = new System.Windows.Forms.Label();
            this.LabelNewX = new System.Windows.Forms.Label();
            this.SegmentXGroup = new System.Windows.Forms.GroupBox();
            this.EndX = new System.Windows.Forms.TextBox();
            this.StartX = new System.Windows.Forms.TextBox();
            this.SaveGraphicDialog = new System.Windows.Forms.SaveFileDialog();
            this.SegmentYGroup = new System.Windows.Forms.GroupBox();
            this.EndY = new System.Windows.Forms.TextBox();
            this.StartY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GraphicBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GraphicField)).BeginInit();
            this.ParametersGroup.SuspendLayout();
            this.GraphicTypeBox.SuspendLayout();
            this.ActionGroup.SuspendLayout();
            this.SegmentXGroup.SuspendLayout();
            this.SegmentYGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // GraphicBox
            // 
            this.GraphicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphicBox.Controls.Add(this.GraphicField);
            this.GraphicBox.Location = new System.Drawing.Point(12, 27);
            this.GraphicBox.Name = "GraphicBox";
            this.GraphicBox.Size = new System.Drawing.Size(671, 515);
            this.GraphicBox.TabIndex = 1;
            this.GraphicBox.TabStop = false;
            this.GraphicBox.Text = "Graphic";
            // 
            // GraphicField
            // 
            this.GraphicField.Cursor = System.Windows.Forms.Cursors.Cross;
            this.GraphicField.Location = new System.Drawing.Point(14, 19);
            this.GraphicField.Name = "GraphicField";
            this.GraphicField.Size = new System.Drawing.Size(640, 480);
            this.GraphicField.TabIndex = 0;
            this.GraphicField.TabStop = false;
            // 
            // EraseButton
            // 
            this.EraseButton.Location = new System.Drawing.Point(79, 61);
            this.EraseButton.Name = "EraseButton";
            this.EraseButton.Size = new System.Drawing.Size(62, 23);
            this.EraseButton.TabIndex = 2;
            this.EraseButton.Text = "Clear";
            this.EraseButton.UseVisualStyleBackColor = true;
            this.EraseButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // DrowGraphButton
            // 
            this.DrowGraphButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DrowGraphButton.Location = new System.Drawing.Point(6, 19);
            this.DrowGraphButton.Name = "DrowGraphButton";
            this.DrowGraphButton.Size = new System.Drawing.Size(135, 36);
            this.DrowGraphButton.TabIndex = 3;
            this.DrowGraphButton.Text = "Draw";
            this.DrowGraphButton.UseVisualStyleBackColor = true;
            this.DrowGraphButton.Click += new System.EventHandler(this.DrowGraphButton_Click);
            // 
            // GraphSelector
            // 
            this.GraphSelector.FormattingEnabled = true;
            this.GraphSelector.Items.AddRange(new object[] {
            "A*Sin(Bx+C)",
            "A*Cos(Bx+C)",
            "A*Tg(Bx+C)",
            "A*Ctg(Bx+C)",
            "Ax^2+Bx+C",
            "|Ax^2+Bx+C|"});
            this.GraphSelector.Location = new System.Drawing.Point(36, 27);
            this.GraphSelector.Name = "GraphSelector";
            this.GraphSelector.Size = new System.Drawing.Size(95, 21);
            this.GraphSelector.TabIndex = 6;
            this.GraphSelector.Text = "A*Sin(Bx+C)";
            this.GraphSelector.SelectedIndexChanged += new System.EventHandler(this.GraphSelector_SelectedIndexChanged);
            // 
            // Label_A
            // 
            this.Label_A.AutoSize = true;
            this.Label_A.Location = new System.Drawing.Point(17, 23);
            this.Label_A.Name = "Label_A";
            this.Label_A.Size = new System.Drawing.Size(25, 13);
            this.Label_A.TabIndex = 7;
            this.Label_A.Text = "a = ";
            // 
            // label_C
            // 
            this.label_C.AutoSize = true;
            this.label_C.Location = new System.Drawing.Point(17, 55);
            this.label_C.Name = "label_C";
            this.label_C.Size = new System.Drawing.Size(25, 13);
            this.label_C.TabIndex = 8;
            this.label_C.Text = "c = ";
            // 
            // label_B
            // 
            this.label_B.AutoSize = true;
            this.label_B.Location = new System.Drawing.Point(81, 26);
            this.label_B.Name = "label_B";
            this.label_B.Size = new System.Drawing.Size(25, 13);
            this.label_B.TabIndex = 9;
            this.label_B.Text = "b = ";
            // 
            // value_A
            // 
            this.value_A.Location = new System.Drawing.Point(40, 23);
            this.value_A.Name = "value_A";
            this.value_A.Size = new System.Drawing.Size(35, 20);
            this.value_A.TabIndex = 10;
            this.value_A.Text = "1";
            // 
            // value_B
            // 
            this.value_B.Location = new System.Drawing.Point(102, 23);
            this.value_B.Name = "value_B";
            this.value_B.Size = new System.Drawing.Size(35, 20);
            this.value_B.TabIndex = 11;
            this.value_B.Text = "1";
            // 
            // value_C
            // 
            this.value_C.Location = new System.Drawing.Point(40, 52);
            this.value_C.Name = "value_C";
            this.value_C.Size = new System.Drawing.Size(35, 20);
            this.value_C.TabIndex = 12;
            this.value_C.Text = "0";
            // 
            // value_Y
            // 
            this.value_Y.AutoSize = true;
            this.value_Y.Location = new System.Drawing.Point(6, 30);
            this.value_Y.Name = "value_Y";
            this.value_Y.Size = new System.Drawing.Size(24, 13);
            this.value_Y.TabIndex = 13;
            this.value_Y.Text = "y = ";
            // 
            // SaveGraphicButton
            // 
            this.SaveGraphicButton.Location = new System.Drawing.Point(6, 61);
            this.SaveGraphicButton.Name = "SaveGraphicButton";
            this.SaveGraphicButton.Size = new System.Drawing.Size(67, 24);
            this.SaveGraphicButton.TabIndex = 15;
            this.SaveGraphicButton.Text = "Save";
            this.SaveGraphicButton.UseVisualStyleBackColor = true;
            this.SaveGraphicButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ParametersGroup
            // 
            this.ParametersGroup.Controls.Add(this.value_A);
            this.ParametersGroup.Controls.Add(this.value_B);
            this.ParametersGroup.Controls.Add(this.Label_A);
            this.ParametersGroup.Controls.Add(this.label_C);
            this.ParametersGroup.Controls.Add(this.label_B);
            this.ParametersGroup.Controls.Add(this.value_C);
            this.ParametersGroup.Location = new System.Drawing.Point(689, 91);
            this.ParametersGroup.Name = "ParametersGroup";
            this.ParametersGroup.Size = new System.Drawing.Size(143, 89);
            this.ParametersGroup.TabIndex = 16;
            this.ParametersGroup.TabStop = false;
            this.ParametersGroup.Text = "Parameters";
            // 
            // GraphicTypeBox
            // 
            this.GraphicTypeBox.Controls.Add(this.GraphSelector);
            this.GraphicTypeBox.Controls.Add(this.value_Y);
            this.GraphicTypeBox.Location = new System.Drawing.Point(689, 27);
            this.GraphicTypeBox.Name = "GraphicTypeBox";
            this.GraphicTypeBox.Size = new System.Drawing.Size(143, 58);
            this.GraphicTypeBox.TabIndex = 18;
            this.GraphicTypeBox.TabStop = false;
            this.GraphicTypeBox.Text = "Type of graphic";
            // 
            // ActionGroup
            // 
            this.ActionGroup.Controls.Add(this.DrowGraphButton);
            this.ActionGroup.Controls.Add(this.SaveGraphicButton);
            this.ActionGroup.Controls.Add(this.EraseButton);
            this.ActionGroup.Location = new System.Drawing.Point(689, 442);
            this.ActionGroup.Name = "ActionGroup";
            this.ActionGroup.Size = new System.Drawing.Size(147, 100);
            this.ActionGroup.TabIndex = 19;
            this.ActionGroup.TabStop = false;
            this.ActionGroup.Text = "Actions";
            // 
            // LabelNewY
            // 
            this.LabelNewY.AutoSize = true;
            this.LabelNewY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelNewY.Location = new System.Drawing.Point(16, 57);
            this.LabelNewY.Name = "LabelNewY";
            this.LabelNewY.Size = new System.Drawing.Size(38, 20);
            this.LabelNewY.TabIndex = 1;
            this.LabelNewY.Text = "End";
            // 
            // LabelNewX
            // 
            this.LabelNewX.AutoSize = true;
            this.LabelNewX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelNewX.Location = new System.Drawing.Point(13, 26);
            this.LabelNewX.Name = "LabelNewX";
            this.LabelNewX.Size = new System.Drawing.Size(48, 20);
            this.LabelNewX.TabIndex = 0;
            this.LabelNewX.Text = "Start ";
            // 
            // SegmentXGroup
            // 
            this.SegmentXGroup.Controls.Add(this.EndX);
            this.SegmentXGroup.Controls.Add(this.StartX);
            this.SegmentXGroup.Controls.Add(this.LabelNewY);
            this.SegmentXGroup.Controls.Add(this.LabelNewX);
            this.SegmentXGroup.Location = new System.Drawing.Point(689, 186);
            this.SegmentXGroup.Name = "SegmentXGroup";
            this.SegmentXGroup.Size = new System.Drawing.Size(143, 97);
            this.SegmentXGroup.TabIndex = 21;
            this.SegmentXGroup.TabStop = false;
            this.SegmentXGroup.Text = "Set X segment";
            // 
            // EndX
            // 
            this.EndX.Location = new System.Drawing.Point(55, 57);
            this.EndX.Name = "EndX";
            this.EndX.Size = new System.Drawing.Size(51, 20);
            this.EndX.TabIndex = 3;
            this.EndX.Text = "5";
            // 
            // StartX
            // 
            this.StartX.Location = new System.Drawing.Point(55, 28);
            this.StartX.Name = "StartX";
            this.StartX.Size = new System.Drawing.Size(51, 20);
            this.StartX.TabIndex = 2;
            this.StartX.Text = "-5";
            // 
            // SaveGraphicDialog
            // 
            this.SaveGraphicDialog.FileName = "graphic";
            this.SaveGraphicDialog.Filter = "JPG files (*.jpg)|*.jpg";
            this.SaveGraphicDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveGraphicDialog_FileOk);
            // 
            // SegmentYGroup
            // 
            this.SegmentYGroup.Controls.Add(this.EndY);
            this.SegmentYGroup.Controls.Add(this.StartY);
            this.SegmentYGroup.Controls.Add(this.label1);
            this.SegmentYGroup.Controls.Add(this.label2);
            this.SegmentYGroup.Location = new System.Drawing.Point(686, 289);
            this.SegmentYGroup.Name = "SegmentYGroup";
            this.SegmentYGroup.Size = new System.Drawing.Size(143, 97);
            this.SegmentYGroup.TabIndex = 22;
            this.SegmentYGroup.TabStop = false;
            this.SegmentYGroup.Text = "Set Y segment";
            // 
            // EndY
            // 
            this.EndY.Location = new System.Drawing.Point(55, 57);
            this.EndY.Name = "EndY";
            this.EndY.Size = new System.Drawing.Size(51, 20);
            this.EndY.TabIndex = 3;
            this.EndY.Text = "5";
            // 
            // StartY
            // 
            this.StartY.Location = new System.Drawing.Point(55, 28);
            this.StartY.Name = "StartY";
            this.StartY.Size = new System.Drawing.Size(51, 20);
            this.StartY.TabIndex = 2;
            this.StartY.Text = "-5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(16, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "End";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(13, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Start ";
            // 
            // MainForm
            // 
            this.AcceptButton = this.DrowGraphButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 549);
            this.Controls.Add(this.SegmentYGroup);
            this.Controls.Add(this.SegmentXGroup);
            this.Controls.Add(this.ActionGroup);
            this.Controls.Add(this.GraphicTypeBox);
            this.Controls.Add(this.ParametersGroup);
            this.Controls.Add(this.GraphicBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Graphic Creator";
            this.TopMost = true;
            this.GraphicBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GraphicField)).EndInit();
            this.ParametersGroup.ResumeLayout(false);
            this.ParametersGroup.PerformLayout();
            this.GraphicTypeBox.ResumeLayout(false);
            this.GraphicTypeBox.PerformLayout();
            this.ActionGroup.ResumeLayout(false);
            this.SegmentXGroup.ResumeLayout(false);
            this.SegmentXGroup.PerformLayout();
            this.SegmentYGroup.ResumeLayout(false);
            this.SegmentYGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox GraphicField;
        private System.Windows.Forms.GroupBox GraphicBox;
        private System.Windows.Forms.Button EraseButton;
        private System.Windows.Forms.Button DrowGraphButton;
        private System.Windows.Forms.ComboBox GraphSelector;
        private System.Windows.Forms.Label Label_A;
        private System.Windows.Forms.Label label_C;
        private System.Windows.Forms.Label label_B;
        private System.Windows.Forms.TextBox value_A;
        private System.Windows.Forms.TextBox value_B;
        private System.Windows.Forms.TextBox value_C;
        private System.Windows.Forms.Label value_Y;
        private System.Windows.Forms.Button SaveGraphicButton;
        private System.Windows.Forms.GroupBox ParametersGroup;
        private System.Windows.Forms.GroupBox GraphicTypeBox;
        private System.Windows.Forms.GroupBox ActionGroup;
        private System.Windows.Forms.Label LabelNewY;
        private System.Windows.Forms.Label LabelNewX;
        private System.Windows.Forms.GroupBox SegmentXGroup;
        private System.Windows.Forms.TextBox EndX;
        private System.Windows.Forms.TextBox StartX;
        private System.Windows.Forms.SaveFileDialog SaveGraphicDialog;
        private System.Windows.Forms.GroupBox SegmentYGroup;
        private System.Windows.Forms.TextBox EndY;
        private System.Windows.Forms.TextBox StartY;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

