﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.ComponentModel;

namespace Graphic_creator
{
    public class Graphic
    {
        private static Bitmap surface;

        public float ZoomFactorX { get; set; }

        public float ZoomFactorY { get; set; }

        private int SegmentStartX { get; set; }

        private int SegmentEndX { get; set; }

        private int SegmentStartY { get; set; }

        private int SegmentEndY { get; set; }

        private int defaultWidth { get; set; }

        private int defaultHeight { get; set; }

        private Point offsetPoint = new Point(0, 0);

        private SaveFileDialog SaveDialog = new SaveFileDialog();

        private delegate double PointCalcHandler(double x, double a, double b, double c);

        private readonly Dictionary<GraphType, PointCalcHandler> graphPointCalc = new Dictionary<GraphType, PointCalcHandler>();

        public Graphic(float zoomX, float zoomY, int width, int height)
        {
            CreateCalcDict();
            ZoomFactorX = zoomX;
            ZoomFactorY = zoomY;
            defaultWidth = width;
            defaultHeight = height;
            if ((zoomX <= 0) || (defaultHeight <= 0) || (defaultWidth <= 0))
            {
                throw new Exception("Parameters can't be negative or null");
            }
        }

        public Graphic(float zoomX, float zoomY, int width, int height, Point point)
        {
            CreateCalcDict();
            ZoomFactorX = zoomX;
            ZoomFactorY = zoomY;
            defaultWidth = width;
            defaultHeight = height;
            offsetPoint = point;
            if ((zoomX <= 0) || (defaultHeight <= 0) || (defaultWidth <= 0))
            {
                throw new Exception("Parameters can't be negative or null");
            }
        }

        private void CreateCalcDict()
        {
            graphPointCalc.Add(GraphType.Cos, (x, a, b, c) => a * Math.Cos(b * x + c));
            graphPointCalc.Add(GraphType.Sin, (x, a, b, c) => a * Math.Sin(b * x + c));
            graphPointCalc.Add(GraphType.Tan, (x, a, b, c) => a * Math.Tan(b * x + c));
            graphPointCalc.Add(GraphType.Ctan, (x, a, b, c) => a / Math.Tan(b * x + c));
            graphPointCalc.Add(GraphType.Mod, (x, a, b, c) => Math.Abs(a * x * x + b * x + c));
            graphPointCalc.Add(GraphType.Quad, (x, a, b, c) => a * x * x + b * x + c);
        }

        public Bitmap DrawAxes()
        {
            surface = new Bitmap(defaultWidth, defaultHeight);
            Graphics grfx = Graphics.FromImage(surface);

            var width = defaultWidth;
            var height = defaultHeight;
            //Drow axes
            var CenterX = width / 2;
            var CenterY = height / 2;

            grfx.DrawLine(Pens.Black, CenterX, 0, CenterX, height);
            grfx.DrawLine(Pens.Black, 0, CenterY, width, CenterY);

            Font myFont = new Font("Calibri", 9, FontStyle.Regular);
            //Drow point tags
            int PointX = SegmentStartX;
            int PointY = SegmentEndY;

            for (int i = 0; i <= Math.Abs(SegmentEndX - SegmentStartX); i++)
            {
                grfx.DrawLine(Pens.Black, i * ZoomFactorX, CenterY + 5, i * ZoomFactorX, CenterY - 5);
                grfx.DrawString(PointX.ToString(), myFont, Brushes.Black, i * ZoomFactorX - 5, CenterY + 10);
                PointX++;
            }

            for (int i = 0; i <= Math.Abs(SegmentEndY - SegmentStartY); i++)
            {
                grfx.DrawLine(Pens.Black, CenterX + 5, i * ZoomFactorY, CenterX - 5, i * ZoomFactorY);
                grfx.DrawString(PointY.ToString(), myFont, Brushes.Black, CenterX + 10, i * ZoomFactorY - 5);
                PointY--;
            }


            return surface;
        }

        public Bitmap GetGraphic(GraphType type, double a, double b, double c)
        {
            Graphics grfx = Graphics.FromImage(DrawAxes());

            var startY = offsetPoint.Y;

            var width = defaultWidth;
            var height = defaultHeight;

            var calc = graphPointCalc[type];

            var CenterY = (height + 2 * startY * ZoomFactorY) / 2;

            const float stepSize = 0.001F;

            for (double x = 0; x <= Math.Abs(SegmentEndX - SegmentStartX) ; x+=stepSize)
            {
                double y = -1 * calc(x + SegmentStartX, a, b, c) + SegmentEndY;

                if (y < SegmentEndY * ZoomFactorY)
                {
                    grfx.FillRectangle(Brushes.Red, (float)(ZoomFactorX * x), (float)(ZoomFactorY * y), 1, 1);
                }
            }

            return surface;
        }

        public void SetOffsetPoint(Point point)
        {
            offsetPoint = point;
        }

        public void GetSegmentX(int Start, int End)
        {
            SegmentStartX = Start;
            SegmentEndX = End;   
        }

        public void GetSegmentY(int Start, int End)
        {
            SegmentStartY = Start;
            SegmentEndY = End;
        }


        public enum GraphType
        {
            Sin,
            Cos,
            Tan,
            Ctan,
            Mod,
            Quad,
        };

    }
}